﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicDasar
{
    public class Logic02Soal04
    {
        public Logic02Soal04() { }

        public static void CetakData(int n)
        {
            for (int i = 0; i < n; i++)
            {
                int angka = 1;

                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        Console.Write(angka + "\t");
                        angka += 2;
                    }
                    else if (i == n - 1)
                    {
                        Console.Write(angka + "\t");
                        angka += 2;

                    }
                    else if (j == 0)
                    {
                        Console.Write(angka + "\t");

                    }
                    else if (j == n - 1)
                    {
                        Console.Write( 17 + "\t");
                    }
                    else if (i == n / 2 || j == n / 2)
                    {
                        Console.Write(0 + "\t");

                    }
                    else
                    {
                        Console.Write("\t");
                    }
                }
                Console.Write("\n");
            }

        }
    }
}
