﻿using LogicDasar;

public class MainLogic
{
    public static void Main()
    {
        Console.WriteLine("Masukkan Angka : ");
        String? input = Console.ReadLine();
        int n = input == null ? 0 : int.Parse(input);

        Logic02Soal01.CetakData(n);
        Console.WriteLine("\n");
        Logic02Soal02.CetakData(n);
        Console.WriteLine("\n");
        Logic02Soal03.CetakData(n);
        Console.WriteLine("\n");
        Logic02Soal04.CetakData(n);
        Console.WriteLine("\n");
        Logic02Soal09.CetakData(n);
        

    }
}