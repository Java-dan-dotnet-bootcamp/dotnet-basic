﻿

public class Declarative
{
    public static void SimpleStatement()
    {
        Console.WriteLine("Declarative Test");

        double area = 0.0;
        Console.WriteLine("Area :" + area);

       double pi = 3.14159;
        Console.WriteLine("pi : "+ pi);

        double radius = 2;
        Console.WriteLine("radius : "+ radius);
    }
}
