﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statement
{
    public class ForStatement
    {
        public static void ForExample()
        {
            Console.WriteLine("Masukkan Angka : " );
            String? nStr = Console.ReadLine();

            int n = nStr == null ? 0 : int.Parse(nStr);

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == j || i + j == n - 1)
                    {
                        Console.Write("[" + i + "," + j + "]\t");
                    }
                    else
                    {
                        Console.Write("\t");
                    }

                }
                Console.Write("\n");
            } 
        
           /* for (int i = 0; i < n; i++)
            {
                int angka = 1;

                for (int j = 0; j < n; j++)
                {
                    if (i == 0 || i == n - 1) 
                    { 
                        Console.WriteLine("[" + i + "," + j + "] \t");
                    angka += 2;
                } else if ( j == 0)
                    {
                        Console.WriteLine("[" + i + "," + j + "] \t");
                    }
                    else if( j == n - 1)
                    {
                        Console.WriteLine(17);

                    }
                }
            } */
        
        }

    }
}
