﻿using DataTypes;
using DataTypes.CustomTypes;
using DataTypes.Inheritance;
using DataTypes.ObjectTypes;
using DataTypes.OOP;
using DataTypes.Polymorphism;
using System.Security.Principal;

public struct Coords
{
    public int x, y;

    public Coords(int p1, int p2)
    {
        x = p1;
        y = p2;
    }

    public override string ToString()
    {
        return "Coord (" + x + "," + y + ")";
    }

}
public class MainDataTypes
{

    public static void Main()
    {
        InterfaceImplement();
        
        Console.WriteLine("=========================================");
       


    }

    #region

    public static void InterfaceImplement()
    {
        SampleClassImplement sample = new SampleClassImplement();
        IControl control = sample;
        ISurface surface = sample;

        // The following lines all call the same method.
        sample.Paint();
        sample.Create();
        sample.Draw();

        control.Paint();
        control.Create();

        surface.Paint();
        surface.Draw();

        // Output:
        // Paint method in SampleClass
        // Paint method in SampleClass
        // Paint method in SampleClass
    }


    #endregion

    #region Sample Polymorphism

    public static void SamplePoly()
    {
        // Polymorphism at work #1: a Rectangle, Triangle and Circle
        // can all be used wherever a Shape is expected. No cast is
        // required because an implicit conversion exists from a derived
        // class to its base class.
        var shapes = new List<Shape>
{
    new Rectangle(),
    new Triangle(),
    new Circle()
};

        // Polymorphism at work #2: the virtual method Draw is
        // invoked on each of the derived classes, not the base class.
        foreach (var shape in shapes)
        {
            shape.Draw();
        }
        /* Output:
            Drawing a rectangle
            Performing base class drawing tasks
            Drawing a triangle
            Performing base class drawing tasks
            Drawing a circle
            Performing base class drawing tasks
        */
    }

    #endregion

    #region Sample Inheritance

    public static void SampleInheritance()
    {
        // Create an instance of WorkItem by using the constructor in the
        // base class that takes three arguments.
        WorkItem item = new WorkItem("Fix Bugs",
                                    "Fix all bugs in my code branch",
                                    new TimeSpan(3, 4, 0, 0));

        // Create an instance of ChangeRequest by using the constructor in
        // the derived class that takes four arguments.
        ChangeRequest change = new ChangeRequest("Change Base Class Design",
                                                "Add members to the class",
                                                new TimeSpan(4, 0, 0),
                                                1);

        // Use the ToString method defined in WorkItem.
        Console.WriteLine(item.ToString());

        // Use the inherited Update method to change the title of the
        // ChangeRequest object.
        change.Update("Change the Design of the Base Class",
            new TimeSpan(4, 0, 0));

        // ChangeRequest inherits WorkItem's override of ToString.
        Console.WriteLine(change.ToString());
        /* Output:
            1 - Fix Bugs
            2 - Change the Design of the Base Class
        */
    }

    #endregion

    #region Sample Object Person

    public static void PersonTest()
    {
        Person person1 = new Person("Shidqi", 12);
        Console.WriteLine("nama : {0} umur : {1} ", person1.Name, person1.Age);

        Person person2 = person1;
        person2.Name = "Juned";
        person2.Age = 22;

        Console.WriteLine("nama : {0} umur : {1} ", person2.Name, person2.Age);
        Console.WriteLine("nama : {0} umur : {1} ", person1.Name, person1.Age);
    }

    #endregion

    #region Test LineofCredit 
    public static void TestLineOfCredit()
    {
        var lineOfCredit = new LineOfCreditAccount("line of credit", 0, 2000);
        // How much is too much to borrow?
        lineOfCredit.MakeWithdrawal(1000m, DateTime.Now, "Take out monthly advance");
        lineOfCredit.MakeDeposit(50m, DateTime.Now, "Pay back small amount");
        lineOfCredit.MakeWithdrawal(5000m, DateTime.Now, "Emergency funds for repairs");
        lineOfCredit.MakeDeposit(150m, DateTime.Now, "Partial restoration on repairs");
        lineOfCredit.PerformMonthEndTransactions();
        Console.WriteLine(lineOfCredit.GetAccountHistory());
    }
    #endregion

    #region Test GiftCardAccound and InterestEarningAcc
    public static void GiftAndInterestTest()
    {
        var giftCard = new GiftCardAccount("gift card", 100, 50);
        giftCard.MakeWithdrawal(20, DateTime.Now, "get expensive coffee");
        giftCard.MakeWithdrawal(50, DateTime.Now, "buy groceries");
        giftCard.PerformMonthEndTransactions();
        // can make additional deposits:
        giftCard.MakeDeposit(27.50m, DateTime.Now, "add some additional spending money");
        Console.WriteLine(giftCard.GetAccountHistory());

        var savings = new InterestEarningAccount("savings account", 10000);
        savings.MakeDeposit(750, DateTime.Now, "save some money");
        savings.MakeDeposit(1250, DateTime.Now, "Add more savings");
        savings.MakeWithdrawal(250, DateTime.Now, "Needed to pay monthly bills");
        savings.PerformMonthEndTransactions();
        Console.WriteLine(savings.GetAccountHistory());
    }
    #endregion

    #region Test negative balance 
    public static void NegativeBalance()
    {
        var account = new BankAccount("Shidqi", 100);
        Console.WriteLine($"Account {account.Number} was created for {account.Owner} with {account.Balance} initial balance.");

        // Test for a negative balance.
        try
        {
            account.MakeWithdrawal(750, DateTime.Now, "Attempt to overdraw");
        }
        catch (InvalidOperationException e)
        {
            Console.WriteLine("Exception caught trying to overdraw");
            Console.WriteLine(e.ToString());
        }


        // Test that the initial balances must be positive.
        BankAccount invalidAccount;
        try
        {
            invalidAccount = new BankAccount("invalid", -55);
        }
        catch (ArgumentOutOfRangeException e)
        {
            Console.WriteLine("Exception caught creating account with negative balance");
            Console.WriteLine(e.ToString());
            return;
        }
    }
    #endregion

    #region Withdawal and Deposit Sample
    public static void WithdrawalDepositData()
    {
        var account = new BankAccount("Shidqi", 10000);
        Console.WriteLine($"Account {account.Number} was created for {account.Owner} with {account.Balance} initial balance.");
        account.MakeWithdrawal(500, DateTime.Now, "Rent payment");
        Console.WriteLine("Saldo anda setelah memebayar sewa : "+account.Balance);
        account.MakeDeposit(100, DateTime.Now, "Friend paid me back");
        Console.WriteLine("Saldo anda setelah teman membayar hutang : "+account.Balance);
        Console.WriteLine("=========================================");
        Console.WriteLine("Riwayat Transaksi");
        Console.WriteLine(account.GetAccountHistory());
    }
    #endregion

    #region Sample Classes and Object BankAccount
    public static void BankAccountData()
    {
        var account = new BankAccount("Shidqi", 100000000);
        Console.WriteLine($"Account {account.Number} was created for {account.Owner} with {account.Balance} initial balance.");

        var account2 = new BankAccount("Bowser", 1000000);
        Console.WriteLine($"Account {account2.Number} was created for {account2.Owner} with {account2.Balance} initial balance.");

        var account3 = new BankAccount("Luigi", 1000000000);
        Console.WriteLine($"Account {account3.Number} was created for {account3.Owner} with {account3.Balance} initial balance.");

        var account4 = new BankAccount("Mario", 50000);
        Console.WriteLine($"Account {account4.Number} was created for {account4.Owner} with {account4.Balance} initial balance.");
    }
    #endregion

    #region Sample class Custom Type
    public static void SampleClass()
    {
        SampleClass sampleClass;
        //Console.WriteLine("Sample class value: {0}", sampleClass.ToString());

        sampleClass = new SampleClass();
        Console.WriteLine("After call contructor");
        Console.WriteLine("Sample class value: {0}", sampleClass.ToString());

        sampleClass = new SampleClass(3, 6);
        Console.WriteLine("After call contructor with params");
        Console.WriteLine("Sample class value: {0}", sampleClass.ToString());
    }
    #endregion


    #region Sample Enum Data Types
    public static void SampleEnumDataTypes()
    {
        Type weekDays = typeof(EnumDays);

        foreach (var item in Enum.GetNames(weekDays))
        {
            Console.WriteLine("Days {0} : " + item);
        }

        Type fielMode = typeof(EnumFieldModel);

        foreach (var item in Enum.GetNames(fielMode))
        {
            Console.WriteLine("Field Model {0} : " + item);
        }

        EnumColors myColors = EnumColors.Red | EnumColors.Blue | EnumColors.Yellow;

        Console.WriteLine();
        Console.WriteLine("myColors holds a combination of colors. Namely: {0}", myColors);

        //https://learn.microsoft.com/en-us/dotnet/standard/base-types/enumeration-format-strings
        Console.WriteLine("Color Red: {0}, GetName: {1}", EnumColors.Red.ToString("D"), Enum.GetName(EnumColors.Red));

        Console.WriteLine("Status : {0}, Names: {1}", EnumApprovalStep.Approved.ToString("D"), Enum.GetName(EnumApprovalStep.Approved));





    }
    #endregion

    #region Sample Data Type
    public static void SampleDataType()
    {
        //deklarasi doang
        float temperature;
        string name;
        //MyClass myClass;


        //deklarasi dengan value atau initialisasi
        char firstLetter = 'C';
        var limit = 3;
        int[] source = { 0, 1, 2, 3, 4, 5 };
        var query = from item in source
                    where item <= limit
                    select item;
        Console.WriteLine();
        foreach (var item in query)
        {
            Console.WriteLine("Item Value : " + item);
        }

        var query2 = from item in source
                     where item % 2 == 1
                     select item;
        Console.WriteLine("////");
        foreach (var item in query2)
        {
            Console.WriteLine("Item Value : " + item);
        }

        var query3 = from item in source
                     where item % 2 == 0
                     select item;
        Console.WriteLine("////");
        foreach (var item in query3)
        {
            Console.WriteLine("Item Value : " + item);
        }

        var query4 = from item in source
                     where item % 3 == 0
                     select item;
        Console.WriteLine("////");
        foreach (var item in query4)
        {
            Console.WriteLine("Item Value : " + item);
        }

        var query5 = from item in source
                     where item < 2
                     select item;
        Console.WriteLine("////");
        foreach (var item in query5)
        {
            Console.WriteLine("Item Value : " + item);
        }
    }
    #endregion

    #region Sample Struct Data Type
    public static void SampleStructDataType()
    {
        Coords point1 = new Coords(2, 2);
        Coords point2 = new Coords(1, 3);
        Console.WriteLine("titik : " + point1);
        Console.WriteLine("titik : " + point2);
    }
    #endregion

   
}