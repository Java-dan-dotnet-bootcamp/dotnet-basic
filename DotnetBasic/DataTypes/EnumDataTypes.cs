﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTypes
{
    public enum EnumDays
    {
        Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
    }

    public enum EnumFieldModel
    {
        CreateNew = 1,
        Create = 2,
        Open = 3,
        OpenOrCreate = 4, 
        Truncate = 5,
        Append = 6

    }

    public enum EnumColors
    {
        Red = 1, Blue = 2, Green = 4, Yellow = 8
    }

    public enum EnumApprovalStep
    {
        Submitted = 0,
        Requested = 1,
        Readed = 2,
        Approved = 3, 
        Rejected = 4
    }
    public class EnumDataTypes
    {

    }
}
