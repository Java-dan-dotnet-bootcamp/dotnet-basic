﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTypes.OOP
{
    public class Transaction
    {
        //Properties
        public decimal Amount { get; }
        public DateTime Date { get; }
        public string Notes { get; }

        //constructor
        public Transaction(decimal amount, DateTime date, string note)
        {
            Amount = amount;
            Date = date;
            Notes = note;
        }
    }
}
