﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceCSharp
{
    public class ClassNameExample
    {

        public static void SimpleClassToString()
        {
            EmptyClass sc = new();
            Console.WriteLine(sc.ToString());
        }

    }
}
