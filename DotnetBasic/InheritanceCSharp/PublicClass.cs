﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceCSharp
{
    public class PublicClass
    {
        public void Method1()
        {
            // Method implementation.
        }

    }
    public class B : PublicClass
    { }
    

}
