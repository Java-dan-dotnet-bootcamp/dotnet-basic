﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static InheritanceCSharp.PrivateClass;

namespace InheritanceCSharp
{
    public class PrivateClass
    {
        private int _value = 10;

        public class B : PrivateClass
        {
            public int GetValue()
            {
                return _value;
            }
        }
    }
    public class C : PrivateClass
    {
        //    public int GetValue()
        //    {
        //        return _value;
        //    }
    }


}
