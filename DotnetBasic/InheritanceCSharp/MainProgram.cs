﻿using InheritanceCSharp;
using System.Net.NetworkInformation;

public class MainProgram
{

    public static void Main(string[] args)
    {
        SimpleClass.ImplicitInheritance();
        Console.WriteLine("===============================\n");
        ClassNameExample.SimpleClassToString();
        // The example displays the following output:
        //        EmptyClass
        Console.WriteLine("===============================\n");
        AutomobileExample();
        Console.WriteLine("===============================\n");
        BookExample();
        Console.WriteLine("===============================\n");
        ShapeExample();


    }

    #region Example Shape

    public static void ShapeExample()
    {
        Shape[] shapes = { new Rectangle(10, 12), new Square(5),
                    new Circle(3) };
        foreach (Shape shape in shapes)
        {
            Console.WriteLine($"{shape}: area, {Shape.GetArea(shape)}; " +
                              $"perimeter, {Shape.GetPerimeter(shape)}");
            if (shape is Rectangle rect)
            {
                Console.WriteLine($"   Is Square: {rect.IsSquare()}, Diagonal: {rect.Diagonal}");
                continue;
            }
            if (shape is Square sq)
            {
                Console.WriteLine($"   Diagonal: {sq.Diagonal}");
                continue;
            }
        }


    }
    // The example displays the following output:
    //         Rectangle: area, 120; perimeter, 44
    //            Is Square: False, Diagonal: 15.62
    //         Square: area, 25; perimeter, 20
    //            Diagonal: 7.07
    //         Circle: area, 28.27; perimeter, 18.85
    #endregion

    #region Example Book
    public static void BookExample()
    {
        var book = new Book("The Tempest", "0971655819", "Shakespeare, William",
                           "Public Domain Press");
        ShowPublicationInfo(book);
        book.Publish(new DateTime(2016, 8, 18));
        ShowPublicationInfo(book);

        var book2 = new Book("The Tempest", "Classic Works Press", "Shakespeare, William");
        Console.Write($"{book.Title} and {book2.Title} are the same publication: " +
              $"{((Publication)book).Equals(book2)}");
    }

    public static void ShowPublicationInfo(Publication pub)
    {
        string pubDate = pub.GetPublicationDate();
        Console.WriteLine($"{pub.Title}, " +
                  $"{(pubDate == "NYP" ? "Not Yet Published" : "published on " + pubDate):d} by {pub.Publisher}");
    }

    // The example displays the following output:
    //        The Tempest, Not Yet Published by Public Domain Press
    //        The Tempest, published on 8/18/2016 by Public Domain Press
    //        The Tempest and The Tempest are the same publication: False

    #endregion

    #region Example for Automobile
    public static void AutomobileExample()
    {
        var packard = new Automobile("Packard", "Custom Eight", 1948);
        Console.WriteLine(packard);

        // The example displays the following output:
        //        1948 Packard Custom Eight
    }
    #endregion

    #region Example for Public Class
    public static void PublicClassExample()
    {
        B b = new();
        b.Method1();
    }

    #endregion

    #region Example Private Class
    public static void PrivateExample()
    {
        var b = new PrivateClass.B();
        Console.WriteLine(b.GetValue());
    }

    // The example displays the following output:
    //       10
    #endregion


}